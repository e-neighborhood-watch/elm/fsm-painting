module Position exposing
  ( Position
  , moveAlong
  , mouseDecoder
  )


import Json.Decode as Json


type alias Position =
  ( Float -- X
  , Float -- Y
  )


mouseDecoder : Json.Decoder Position
mouseDecoder =
  Json.map2
    Tuple.pair
    (Json.field "pageX" Json.float)
    (Json.field "pageY" Json.float)


moveAlong : Position -> Position -> Position -> Position
moveAlong ( oldX, oldY ) ( newX, newY ) ( moveX, moveY ) =
  ( newX - oldX + moveX
  , newY - oldY + moveY
  )
