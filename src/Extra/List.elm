module Extra.List exposing
  ( reverseMap
  )


reverseMapHelper : List b -> (a -> b) -> List a -> List b
reverseMapHelper acc f list =
  case
    list
  of
    [] ->
      acc

    head :: tail ->
      reverseMapHelper
        (f head :: acc)
        f
        tail


reverseMap : (a -> b) -> List a -> List b
reverseMap = reverseMapHelper []
