module Node exposing
  ( Node
  , Message (..)
  , render
  , renderSelected
  )


import Css
import Json.Decode as Json
import Svg.Styled as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes as SvgAttr
import Svg.Styled.Events as SvgEvent


import Position
  exposing
    ( Position
    )


type alias Node =
  { position : Position
  }


type Message
  = MouseDown Int Position
  | MouseUp Int
  | SelectedMouseDown Position
  | SelectedMouseUp


style : List Css.Style
style =
  [ Css.property "stroke" "black"
  , Css.property "fill" "white"
  , Css.property "stroke-width" "0.7970133333333332"
  ]


selectedStyle : List Css.Style
selectedStyle =
  [ Css.property "stroke" "purple"
  , Css.property "fill" "white"
  , Css.property "stroke-width" "0.7970133333333332"
  ]


renderSelected : Node -> Svg Message
renderSelected { position } =
  Svg.circle
    [ position |> Tuple.first |> String.fromFloat |> SvgAttr.cx
    , position |> Tuple.second |> String.fromFloat |> SvgAttr.cy
    , SvgAttr.r "23.41146"
    , SvgAttr.css selectedStyle
    , Position.mouseDecoder
      |> Json.map ( SelectedMouseDown >> Tuple.pair >> (|>) True )
      |> SvgEvent.stopPropagationOn "mousedown"
    , Json.succeed ( SelectedMouseUp, True )
      |> SvgEvent.stopPropagationOn "mouseup"
    ]
    []


render : Int -> Node -> Svg Message
render index { position } =
  Svg.circle
    [ position |> Tuple.first |> String.fromFloat |> SvgAttr.cx
    , position |> Tuple.second |> String.fromFloat |> SvgAttr.cy
    , SvgAttr.r "23.41146"
    , SvgAttr.css style
    , Position.mouseDecoder
      |> Json.map ( MouseDown index >> Tuple.pair >> (|>) True )
      |> SvgEvent.stopPropagationOn "mousedown"
    , Json.succeed ( MouseUp index, True )
      |> SvgEvent.stopPropagationOn "mouseup"
    ]
    []
