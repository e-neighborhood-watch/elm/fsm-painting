module Main exposing
  ( main
  )


import Browser
import Browser.Events
import Css
import Css.Global
import Dict
  exposing
    ( Dict
    )
import Html.Styled as Html
  exposing
    ( Html
    )
import Json.Decode as Json
import Svg.Styled as Svg
import Svg.Styled.Attributes as SvgAttr
import Svg.Styled.Events as SvgEvent


import Extra.List as List
import Node
  exposing
    ( Node
    )
import Position
  exposing
    ( Position
    )


type alias Model =
  { nodes : Dict Int Node
  , selectedNode : Maybe Node
  , mouseTarget : MouseTarget
  , nextNodeId : Int
  }


type MouseTarget
  = None
  | Node Position


type Message
  = NodeMessage Node.Message
  | VisibilityChanged Browser.Events.Visibility
  | MouseMove Position
  | MouseUp
  | NewNode Position


init : a -> ( Model, Cmd Message )
init _ =
  ( { nodes =
      Dict.empty
    , selectedNode =
      Nothing
    , mouseTarget =
      None
    , nextNodeId =
      0
    }
  , Cmd.none
  )


update : Message -> Model -> ( Model, Cmd Message )
update message prevModel =
  case
    message
  of
    NodeMessage (Node.MouseDown ix initialPos) ->
      case
        prevModel.selectedNode
      of
        Nothing ->
          ( { nodes =
              Dict.remove ix prevModel.nodes
            , selectedNode =
              Dict.get ix prevModel.nodes
            , mouseTarget =
              Node initialPos
            , nextNodeId =
              prevModel.nextNodeId
            }
          , Cmd.none
          )

        Just prevSelectedNode ->
          ( { nodes =
              prevModel.nodes
                |> Dict.remove ix
                |> Dict.insert prevModel.nextNodeId prevSelectedNode
            , selectedNode =
              Dict.get ix prevModel.nodes
            , mouseTarget =
              Node initialPos
            , nextNodeId =
              prevModel.nextNodeId + 1
            }
          , Cmd.none
          )

    NodeMessage (Node.SelectedMouseDown initialPos) ->
      ( { nodes =
          prevModel.nodes
        , selectedNode =
          prevModel.selectedNode
        , mouseTarget =
          Node initialPos
        , nextNodeId =
          prevModel.nextNodeId
        }
      , Cmd.none
      )

    NodeMessage (Node.MouseUp ix) ->
      case
        prevModel.selectedNode
      of
        Nothing ->
          ( { nodes =
              Dict.remove ix prevModel.nodes
            , selectedNode =
              Dict.get ix prevModel.nodes
            , mouseTarget =
              None
            , nextNodeId =
              prevModel.nextNodeId
            }
          , Cmd.none
          )

        Just prevSelectedNode ->
          ( { nodes =
              prevModel.nodes
                |> Dict.remove ix
                |> Dict.insert prevModel.nextNodeId prevSelectedNode
            , selectedNode =
              Dict.get ix prevModel.nodes
            , mouseTarget =
              None
            , nextNodeId =
              prevModel.nextNodeId + 1
            }
          , Cmd.none
          )

    NodeMessage Node.SelectedMouseUp ->
      ( { nodes =
          prevModel.nodes
        , selectedNode =
          prevModel.selectedNode
        , mouseTarget =
          None
        , nextNodeId =
          prevModel.nextNodeId
        }
      , Cmd.none
      )

    MouseUp ->
      case
        prevModel.selectedNode
      of
        Nothing ->
          ( { nodes =
              prevModel.nodes
            , selectedNode =
              Nothing
            , mouseTarget =
              None
            , nextNodeId =
              prevModel.nextNodeId
            }
          , Cmd.none
          )

        Just prevSelectedNode ->
          ( { nodes =
              Dict.insert prevModel.nextNodeId prevSelectedNode prevModel.nodes
            , selectedNode =
              Nothing
            , mouseTarget =
              None
            , nextNodeId =
              prevModel.nextNodeId + 1
            }
          , Cmd.none
          )

    NewNode pos ->
      case
        prevModel.selectedNode
      of
        Nothing ->
          ( { nodes =
              Dict.insert
                prevModel.nextNodeId
                { position =
                  pos
                }
                prevModel.nodes
            , selectedNode =
              Nothing
            , mouseTarget =
              None
            , nextNodeId =
              prevModel.nextNodeId + 1
            }
          , Cmd.none
          )

        Just prevSelectedNode ->
          ( { nodes =
              prevModel.nodes
                |> Dict.insert prevModel.nextNodeId prevSelectedNode
                |> Dict.insert (prevModel.nextNodeId + 1) { position = pos }
            , selectedNode =
              Nothing
            , mouseTarget =
              None
            , nextNodeId =
              prevModel.nextNodeId + 2
            }
          , Cmd.none
          )

    MouseMove pos ->
      case
        prevModel.mouseTarget
      of
        None ->
          ( prevModel
          , Cmd.none
          )

        Node prevPos ->
          case
            prevModel.selectedNode
          of
            Nothing ->
              ( { nodes =
                  prevModel.nodes
                , selectedNode =
                  prevModel.selectedNode
                , mouseTarget =
                  None
                , nextNodeId =
                  prevModel.nextNodeId
                }
              , Cmd.none
              )

            Just { position } ->
              ( { nodes =
                  prevModel.nodes
                , selectedNode =
                  Just
                    { position =
                      Position.moveAlong
                        prevPos
                        pos
                        position
                    }
                , mouseTarget =
                  Node pos
                , nextNodeId =
                  prevModel.nextNodeId
                }
              , Cmd.none
              )

    VisibilityChanged Browser.Events.Hidden ->
      ( { nodes =
          prevModel.nodes
        , selectedNode =
          prevModel.selectedNode
        , mouseTarget =
          None
        , nextNodeId =
          prevModel.nextNodeId
        }
      , Cmd.none
      )

    VisibilityChanged Browser.Events.Visible ->
      ( prevModel
      , Cmd.none
      )


bodyAndHtmlStyles : Html msg
bodyAndHtmlStyles =
  Css.Global.global
    [ Css.Global.html
      [ Css.height (Css.pct 100)
      ]
    , Css.Global.body
      [ Css.height (Css.pct 100)
      ]
    ]


body : Model -> Html Message
body { nodes, mouseTarget, selectedNode } =
  Html.div
    [ SvgAttr.css
      [ Css.displayFlex
      , Css.top Css.zero
      , Css.left Css.zero
      , Css.width (Css.pct 100)
      , Css.height (Css.pct 100)
      ]
    ]
    [ nodes
        |> Dict.map (Node.render >> (<<) (Svg.map NodeMessage))
        |> Dict.values
        |> (++)
        |>
          (|>)
            ( case
                selectedNode
              of
                Nothing ->
                  []

                Just node ->
                  Node.renderSelected node
                    |> Svg.map NodeMessage
                    |> List.singleton
            )
        |>
          Svg.svg
            [ SvgAttr.css
              [ Css.flex (Css.num 3)
              ]
            , SvgEvent.on "mouseup" (Json.map NewNode Position.mouseDecoder)
            ]
    , bodyAndHtmlStyles
    ]


view : Model -> Browser.Document Message
view model =
  { title =
    "FSM Painting"
  , body =
    body model
      |> Html.toUnstyled
      |> List.singleton
  }


subscriptions : Model -> Sub Message
subscriptions { mouseTarget } =
  case
    mouseTarget
  of
    None ->
      Sub.none

    Node _ ->
      Sub.batch
        [ Browser.Events.onMouseUp (Json.succeed MouseUp)
        , Browser.Events.onVisibilityChange VisibilityChanged
        , Browser.Events.onMouseMove (Json.map MouseMove Position.mouseDecoder)
        ]


main : Program () Model Message
main =
  Browser.document
    { init = init
    , update = update
    , view = view
    , subscriptions = subscriptions
    }
